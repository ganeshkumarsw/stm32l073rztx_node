/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "modbus_common.h"
#include "modbus_slave.h"
#include "modbus_config.h"

/* Private define ------------------------------------------------------------*/
#define MODBUS_BIT_MASK(x)  (1 << x)

/* Private variables ---------------------------------------------------------*/
#if MODBUS_SLAVE
uint8_t Modbus_Server_U8[10] = {0xAA, 0x03};
uint16_t Modbus_Server_U16[1000];
#endif

#ifdef MODBUS_MASTER
uint16_t Modbus_Client_U16_Coil;
uint16_t Modbus_Client_U16[10];
#endif

/* Extern variables ----------------------------------------------------------*/

/* Imported functions---------------------------------------------------------*/
extern void MB_Slave10_CoilCallBack(const uint8_t* p_data);

/* Global variables ----------------------------------------------------------*/
#if MODBUS_SLAVE
const modbus_coil_t Modbus_Server_DescreteInput[MODBUS_MAX_DESCRETE_INPUT] =
{
    {MODBUS_BIT_MASK(0), (uint8_t*)&Modbus_Server_U16[0]},
    {MODBUS_BIT_MASK(1), (uint8_t*)&Modbus_Server_U16[0] + 1},
    {MODBUS_BIT_MASK(2), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(3), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(4), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(5), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(6), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(7), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(0), &Modbus_Server_U8[1]},
    {MODBUS_BIT_MASK(1), &Modbus_Server_U8[1]},
};

const modbus_coil_t Modbus_Server_Coils[MODBUS_MAX_COILS] =
{
    {MODBUS_BIT_MASK(0), (uint8_t*)&Modbus_Server_U16[0]},
    {MODBUS_BIT_MASK(1), (uint8_t*)&Modbus_Server_U16[0] + 1},
    {MODBUS_BIT_MASK(2), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(3), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(4), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(5), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(6), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(7), &Modbus_Server_U8[0]},
    {MODBUS_BIT_MASK(0), &Modbus_Server_U8[1]},
    {MODBUS_BIT_MASK(1), &Modbus_Server_U8[1]},
};

const modbus_reg_t Modbus_Server_HoldingReg[MODBUS_MAX_HOLDING_REG] =
{
    &Modbus_Server_U16[0],
    &Modbus_Server_U16[1],
    &Modbus_Server_U16[2],
    &Modbus_Server_U16[3],
    &Modbus_Server_U16[4],
    &Modbus_Server_U16[5],
    &Modbus_Server_U16[6],
    &Modbus_Server_U16[7],
    &Modbus_Server_U16[8],
    &Modbus_Server_U16[9],
    &Modbus_Server_U16[10],
    &Modbus_Server_U16[11],
    &Modbus_Server_U16[12],
    &Modbus_Server_U16[13],
    &Modbus_Server_U16[14],
    &Modbus_Server_U16[15],
    &Modbus_Server_U16[16],
    &Modbus_Server_U16[17],
    &Modbus_Server_U16[18],
    &Modbus_Server_U16[19],
    &Modbus_Server_U16[20],
    &Modbus_Server_U16[21],
    &Modbus_Server_U16[22],
    &Modbus_Server_U16[23],
    &Modbus_Server_U16[24],
    &Modbus_Server_U16[25],
    &Modbus_Server_U16[26],
    &Modbus_Server_U16[27],
    &Modbus_Server_U16[28],
    &Modbus_Server_U16[29],
    &Modbus_Server_U16[30],
    &Modbus_Server_U16[31],
    &Modbus_Server_U16[32],
    &Modbus_Server_U16[33],
    &Modbus_Server_U16[34],
    &Modbus_Server_U16[35],
    &Modbus_Server_U16[36],
    &Modbus_Server_U16[37],
    &Modbus_Server_U16[38],
    &Modbus_Server_U16[39],
    &Modbus_Server_U16[40],
    &Modbus_Server_U16[41],
    &Modbus_Server_U16[42],
    &Modbus_Server_U16[43],
    &Modbus_Server_U16[44],
    &Modbus_Server_U16[45],
    &Modbus_Server_U16[46],
    &Modbus_Server_U16[47],
    &Modbus_Server_U16[48],
    &Modbus_Server_U16[49],
    &Modbus_Server_U16[50],
    &Modbus_Server_U16[51],
    &Modbus_Server_U16[52],
    &Modbus_Server_U16[53],
    &Modbus_Server_U16[54],
    &Modbus_Server_U16[55],
    &Modbus_Server_U16[56],
    &Modbus_Server_U16[57],
    &Modbus_Server_U16[58],
    &Modbus_Server_U16[59],
    &Modbus_Server_U16[60],
    &Modbus_Server_U16[61],
    &Modbus_Server_U16[62],
    &Modbus_Server_U16[63],
    &Modbus_Server_U16[64],
    &Modbus_Server_U16[65],
    &Modbus_Server_U16[66],
    &Modbus_Server_U16[67],
    &Modbus_Server_U16[68],
    &Modbus_Server_U16[69],
    &Modbus_Server_U16[70],
    &Modbus_Server_U16[71],
    &Modbus_Server_U16[72],
    &Modbus_Server_U16[73],
    &Modbus_Server_U16[74],
    &Modbus_Server_U16[75],
    &Modbus_Server_U16[76],
    &Modbus_Server_U16[77],
    &Modbus_Server_U16[78],
    &Modbus_Server_U16[79],
    &Modbus_Server_U16[80],
    &Modbus_Server_U16[81],
    &Modbus_Server_U16[82],
    &Modbus_Server_U16[83],
    &Modbus_Server_U16[84],
    &Modbus_Server_U16[85],
    &Modbus_Server_U16[86],
    &Modbus_Server_U16[87],
    &Modbus_Server_U16[88],
    &Modbus_Server_U16[89],
    &Modbus_Server_U16[90],
    &Modbus_Server_U16[91],
    &Modbus_Server_U16[92],
    &Modbus_Server_U16[93],
    &Modbus_Server_U16[94],
    &Modbus_Server_U16[95],
    &Modbus_Server_U16[96],
    &Modbus_Server_U16[97],
    &Modbus_Server_U16[98],
    &Modbus_Server_U16[99],
    &Modbus_Server_U16[100],
};

const modbus_reg_t Modbus_Server_InputReg[MODBUS_MAX_INPUT_REG] =
{
    &Modbus_Server_U16[0],
    &Modbus_Server_U16[1],
    &Modbus_Server_U16[2],
    &Modbus_Server_U16[3],
    &Modbus_Server_U16[4],
    &Modbus_Server_U16[5],
    &Modbus_Server_U16[6],
    &Modbus_Server_U16[7],
    &Modbus_Server_U16[8],
    &Modbus_Server_U16[9],
};
#endif

#ifdef MODBUS_MASTER
const modbus_telegram_t Modbus_Client_Telegram[MODBUS_MAX_TELEGRAM] =
{
    {1, MODBUS_FUNC_READ_INPUT_REG, 0, 2, &Modbus_Client_U16[0], NULL},
    //{10, MODBUS_FUNC_WRITE_SINGLE_REG, 0, 1, &Modbus_Client_U16[0], NULL},
    //{10, MODBUS_FUNC_WRITE_SINGLE_REG, 1, 1, &Modbus_Client_U16[1], NULL},
    //{10, MODBUS_FUNC_READ_HOLDING_REG, 0, 2, &Modbus_Client_U16[2], NULL},
    //{10, MODBUS_FUNC_READ_DISCRETE_INPUTS, 0, 8, &Modbus_Client_U16[4], MB_Slave10_CoilCallBack},
    //{10, MODBUS_FUNC_WRITE_SINGLE_COILS, 0, 1, &Modbus_Client_U16_Coil, NULL},
    //{10, MODBUS_FUNC_READ_COILS, 0, 8, &Modbus_Client_U16[5], NULL},

};
#endif
/* Private function prototypes -----------------------------------------------*/
