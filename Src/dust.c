/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "dust.h"

/* Global variables ----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

/* Private function prototypes -----------------------------------------------*/
void DUST_Init(void)
{
    HAL_TIM_IC_Start(&htim2, TIM_CHANNEL_2);
    HAL_TIM_IC_Start(&htim3, TIM_CHANNEL_2);
}

uint16_t DUST_Read_P1(void)
{
    return ((uint16_t)HAL_TIM_ReadCapturedValue(&htim2, TIM_CHANNEL_2));
}

uint16_t DUST_Read_P2(void)
{
    return ((uint16_t)HAL_TIM_ReadCapturedValue(&htim3, TIM_CHANNEL_2));
}
