/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "iaq.h"

/* Global variables ----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c1;

/* Private function prototypes -----------------------------------------------*/

uint8_t IAQ_Read(iaq_read_t type, void* p_outData)
{
    uint8_t buff[9];
    uint8_t status;

    status = 0;

    if(HAL_I2C_Master_Receive(&hi2c1, IAQ_READ_ADDR, buff, sizeof(buff), 10) == HAL_OK)
    {
        status = 1;

        if(buff[2] == 0)
        {
            if(type == IAQ_READ_CO2)
            {
                *(uint16_t*)p_outData = ((uint16_t)buff[0] << 8) | (uint16_t)buff[1];
            }
            else if(type == IAQ_READ_RESISTANCE)
            {
                *(uint32_t*)p_outData = ((uint32_t)buff[4] << 16) | ((uint32_t)buff[5] << 8) | (uint32_t)buff[6];
            }
            else if(type == IAQ_READ_TVOC)
            {
                *(uint16_t*)p_outData = ((uint16_t)buff[7] << 8) | (uint16_t)buff[8];
            }
        }
        else
        {
            status = buff[2];
        }
    }

    return status;
}
