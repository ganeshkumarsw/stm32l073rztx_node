
/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/**
 * 
 * @param timerId
 * @param ms
 */
void Timer_Start(timer_t *p_swTimer, uint32_t delay)
{
    // copy the duration
    p_swTimer->delay = delay;
    // copy the current systick to the timestamp, this will be used to check whether the
    // software is elapsed or still running
    p_swTimer->timeStamp = HAL_GetTick();
}

/**
 * 
 * @param timerId
 */
void Timer_Stop(timer_t *p_swTimer)
{
    // Reset the delay to 0 which internally means the software timer is stopped
    p_swTimer->delay = 0;
    // reset the timestamp to 0
    p_swTimer->timeStamp = 0;
}

/**
 * 
 * @param timerId
 * @return 
 */
timer_status_t Timer_GetStatus(timer_t *p_swTimer)
{
    timer_status_t status;
    uint32_t tick;

    // get the current systick
    tick = HAL_GetTick();

    // Check if the software timer is still running
    if(p_swTimer->delay)
    {
        // Check if the systick is overflowed or not
        if(tick < p_swTimer->timeStamp)
        {
            // Do the overflow correction
            // Check if the software timer is elapsed or not
            if(((0xFFFFFFFF - p_swTimer->timeStamp) + tick) > p_swTimer->delay)
            {
                // software timer is elapsed
                status = TIMER_ELAPSED;
            }
            else
            {
                // software timer is still running
                status = TIMER_RUNNING;
            }
        }
        else
        {
            // Check if the software timer is elapsed or not
            if((tick - p_swTimer->timeStamp) > p_swTimer->delay)
            {
                // software timer is elapsed
                status = TIMER_ELAPSED;
            }
            else
            {
                // software timer is still running
                status = TIMER_RUNNING;
            }
        }
    }
    else
    {
        // software timer is stopped
        status = TIMER_STOPPED;
    }

    // return the status of the software timer
    return status;
}
