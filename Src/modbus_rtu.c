/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "modbus_common.h"
#include "modbus_slave.h"
#include "modbus_master.h"
#include "modbus_config.h"
#include "modbus_rtu.h"

/* Global variables ----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

void Modbus_RTU_Init(void)
{
#if MODBUS_SLAVE
    Modbus_SlaveInit();
#endif
    
#if MODBUS_MASTER
    Modbus_MasterInit();
#endif
}

/**
 *
 */
void Modbus_RTU_Job(void)
{
#if MODBUS_SLAVE
    Modbus_SlaveJob();
#endif
    
#if MODBUS_MASTER
    Modbus_MasterJob();
#endif
}

/**
 *
 * @param p_buf
 * @param len
 * @return
 */
uint16_t Modbus_CRC16_Calc(uint8_t* p_buf, uint16_t len)
{
    uint16_t crc = 0xFFFF;
    uint16_t pos;
    uint8_t i;

    for(pos = 0;pos < len;pos++)
    {
        crc ^= (uint16_t)p_buf[pos];          // XOR byte into least sig. byte of crc

        for(i = 8;i != 0;i--)
        {
            // Loop over each bit
            if((crc & 0x0001) != 0)
            {
                // If the LSB is set
                crc >>= 1;                    // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else
            {
                // Else LSB is not set
                crc >>= 1;                    // Just shift right
            }
        }
    }
    // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
    return crc;
}

/**
 *
 * @param huart
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
#if MODBUS_SLAVE
    if(huart == &MODBUS_SLAVE_UART)
    {
        Modbus_Slave_TxCpltdCallback();
    }
#endif
    
#if MODBUS_MASTER
    if(huart == &MODBUS_MSTR_UART)
    {
        Modbus_Master_TxCpltdCallback();
    }
#endif
}
