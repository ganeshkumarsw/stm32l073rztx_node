/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "modbus_common.h"
#include "modbus_config.h"
#include "modbus_master.h"

#if MODBUS_MASTER

/* Global variables ----------------------------------------------------------*/
modbus_master_state_t Modbus_MasterState;
modbus_frame_t Modbus_MasterFrame;
timer_t Modbus_MasterRespTimer;
timer_t Modbus_MasterNoRespTimer;
timer_t Modbus_MasterPollTimer;
uint8_t Modbus_MasterTelegramId;

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

static uint8_t Modbus_MasterProcessFrame(uint8_t telegramId, modbus_frame_t* p_frame);

/**
 *
 */
void Modbus_MasterInit(void)
{
#if MODBUS_MSTR_OVER_RS485
    HAL_GPIO_WritePin(MODBUS_MSTR_RE_TE_PORT, MODBUS_MSTR_RE_TE_PIN, GPIO_PIN_RESET);
#endif

    HAL_UART_Receive_IT(&MODBUS_MSTR_UART, Modbus_MasterFrame.xferBuff, sizeof(Modbus_MasterFrame.xferBuff));
    
    Timer_Stop(&Modbus_MasterRespTimer);
    Timer_Stop(&Modbus_MasterNoRespTimer);
    Timer_Stop(&Modbus_MasterPollTimer);
    Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
    Modbus_MasterTelegramId = 0;
}

void Modbus_MasterJob(void)
{
    uint16_t idx;
    uint16_t len;
    uint16_t crc;
    modbus_func_t funcCode;
    timer_t waitTimer;

    len = MODBUS_MSTR_UART.RxXferSize - MODBUS_MSTR_UART.RxXferCount;

    switch(Modbus_MasterState)
    {
        case MODBUS_MSTR_STATE_REQ_FRAME:

#if MODBUS_MSTR_OVER_RS485
            HAL_GPIO_WritePin(MODBUS_MSTR_RE_TE_PORT, MODBUS_MSTR_RE_TE_PIN, GPIO_PIN_RESET);
#endif
            idx = 0;
            Modbus_MasterTelegramId++;
            if(Modbus_MasterTelegramId >= MODBUS_MAX_TELEGRAM)
            {
                Modbus_MasterTelegramId = 0;
            }
            Modbus_MasterFrame.xferBuff[idx++] = Modbus_Client_Telegram[Modbus_MasterTelegramId].slaveAddr;
            Modbus_MasterFrame.xferBuff[idx++] = Modbus_Client_Telegram[Modbus_MasterTelegramId].funcCode;
            Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)(Modbus_Client_Telegram[Modbus_MasterTelegramId].addr >> 8);
            Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)Modbus_Client_Telegram[Modbus_MasterTelegramId].addr;

            if((Modbus_Client_Telegram[Modbus_MasterTelegramId].funcCode == MODBUS_FUNC_WRITE_SINGLE_REG)
               || (Modbus_Client_Telegram[Modbus_MasterTelegramId].funcCode == MODBUS_FUNC_WRITE_SINGLE_COILS))
            {
                Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)(*(uint16_t*)Modbus_Client_Telegram[Modbus_MasterTelegramId].p_data >> 8);
                Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)(*(uint16_t*)Modbus_Client_Telegram[Modbus_MasterTelegramId].p_data);
            }
            else
            {
                Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)(Modbus_Client_Telegram[Modbus_MasterTelegramId].qty >> 8);
                Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)Modbus_Client_Telegram[Modbus_MasterTelegramId].qty;
            }

            crc = Modbus_CRC16_Calc(Modbus_MasterFrame.xferBuff, idx);
            Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)crc;
            Modbus_MasterFrame.xferBuff[idx++] = (uint8_t)(crc >> 8);
            Modbus_MasterFrame.len = idx;

            Timer_Start(&waitTimer, 200);
            while(Timer_GetStatus(&waitTimer) != TIMER_ELAPSED)
            {

            }

            Timer_Start(&Modbus_MasterNoRespTimer, 200);

            HAL_UART_Transmit_IT(&MODBUS_MSTR_UART, Modbus_MasterFrame.xferBuff, Modbus_MasterFrame.len);
            Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME_IN_PROGRESS;
            break;

        case MODBUS_MSTR_STATE_REQ_FRAME_IN_PROGRESS:
            // Transmit is in progress through interrupt mechanism
            break;

        case MODBUS_MSTR_STATE_WAIT_FOR_RESP_FRAME:

            if(len == 0)
            {
                if(Timer_GetStatus(&Modbus_MasterNoRespTimer) == TIMER_ELAPSED)
                {
                    Timer_Stop(&Modbus_MasterNoRespTimer);
                    Modbus_MasterFlushBuffer();
                    Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
                }
            }
            else if(len < 3)
            {
                Timer_Stop(&Modbus_MasterNoRespTimer);

                if(Timer_GetStatus(&Modbus_MasterRespTimer) == TIMER_STOPPED)
                {
                    Timer_Start(&Modbus_MasterRespTimer, MODBUS_MASTER_RESP_HEADER_TIMEOUT);
                }

                if(Timer_GetStatus(&Modbus_MasterRespTimer) == TIMER_ELAPSED)
                {
                    Timer_Stop(&Modbus_MasterRespTimer);
                    Modbus_MasterFlushBuffer();
                    Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
                }
            }
            else if(len > 3)
            {
                Timer_Stop(&Modbus_MasterRespTimer);

                funcCode = (modbus_func_t)Modbus_MasterFrame.xferBuff[1];

                if((funcCode == MODBUS_FUNC_READ_DISCRETE_INPUTS)
                   || (funcCode == MODBUS_FUNC_READ_COILS)
                   || (funcCode == MODBUS_FUNC_READ_HOLDING_REG)
                   || (funcCode == MODBUS_FUNC_READ_INPUT_REG))
                {
                    Modbus_MasterFrame.len = 3 + Modbus_MasterFrame.xferBuff[2] + 2;
                }
                else if((funcCode == MODBUS_FUNC_WRITE_SINGLE_COILS) || (funcCode == MODBUS_FUNC_WRITE_SINGLE_REG))
                {
                    Modbus_MasterFrame.len = 2 + 2 + 2 + 2;
                }
                else
                {
                    Modbus_MasterFrame.len = 5;
                }

                Timer_Start(&Modbus_MasterRespTimer, Modbus_MasterFrame.len);

                Modbus_MasterState = MODBUS_MSTR_STATE_RESP_FRAME_IN_PROGRESS;
            }
            break;

        case MODBUS_MSTR_STATE_RESP_FRAME_IN_PROGRESS:

            if(len == Modbus_MasterFrame.len)
            {
                Timer_Stop(&Modbus_MasterRespTimer);
                Modbus_MasterState = MODBUS_MSTR_STATE_RESP_FRAME_RECEIVED;
            }
            else if(Timer_GetStatus(&Modbus_MasterRespTimer) == TIMER_ELAPSED)
            {
                Timer_Stop(&Modbus_MasterRespTimer);
                Modbus_MasterFlushBuffer();

                Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
            }

            break;

        case MODBUS_MSTR_STATE_RESP_FRAME_RECEIVED:

            crc = ((uint16_t)Modbus_MasterFrame.xferBuff[Modbus_MasterFrame.len - 1] << 8)
                  | (uint16_t)Modbus_MasterFrame.xferBuff[Modbus_MasterFrame.len - 2];

            if(crc == Modbus_CRC16_Calc(Modbus_MasterFrame.xferBuff, Modbus_MasterFrame.len - 2))
            {
                Modbus_MasterState = MODBUS_MSTR_STATE_PROCESS_RESP_FRAME;
            }
            else
            {
                Modbus_MasterFlushBuffer();
                Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
            }
            break;

        case MODBUS_MSTR_STATE_PROCESS_RESP_FRAME:

            if(Modbus_MasterProcessFrame(Modbus_MasterTelegramId, &Modbus_MasterFrame) == 1)
            {
                Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
            }
            else
            {
                Modbus_MasterFlushBuffer();
                Modbus_MasterState = MODBUS_MSTR_STATE_REQ_FRAME;
            }

#if MODBUS_MSTR_OVER_RS485
            HAL_GPIO_WritePin(MODBUS_MSTR_RE_TE_PORT, MODBUS_MSTR_RE_TE_PIN, GPIO_PIN_RESET);
#endif
            break;

        default:
            break;
    }
}

void Modbus_MasterFlushBuffer(void)
{
    /* Disable the UART Data Register not empty Interrupt */
    __HAL_UART_DISABLE_IT(&MODBUS_MSTR_UART, UART_IT_RXNE);

    MODBUS_MSTR_UART.ErrorCode = HAL_UART_ERROR_NONE;
    MODBUS_MSTR_UART.pRxBuffPtr = Modbus_MasterFrame.xferBuff;
    MODBUS_MSTR_UART.RxXferSize = sizeof(Modbus_MasterFrame.xferBuff);
    MODBUS_MSTR_UART.RxXferCount = sizeof(Modbus_MasterFrame.xferBuff);

    if(MODBUS_MSTR_UART.RxState == HAL_UART_STATE_READY)
    {
        MODBUS_MSTR_UART.RxState = HAL_UART_STATE_BUSY_RX;
    }
    
    /* Clear all the error flag at once */
    __HAL_UART_CLEAR_PEFLAG(&MODBUS_MSTR_UART);

    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&MODBUS_MSTR_UART, UART_IT_RXNE);
}

void Modbus_Master_TxCpltdCallback(void)
{
    Modbus_MasterFlushBuffer();
#if MODBUS_MSTR_OVER_RS485
    HAL_GPIO_WritePin(MODBUS_MSTR_RE_TE_PORT, MODBUS_MSTR_RE_TE_PIN, GPIO_PIN_SET);
#endif

    Modbus_MasterState = MODBUS_MSTR_STATE_WAIT_FOR_RESP_FRAME;
}

/**
 *
 * @param telegramId
 * @param p_frame
 * @return
 */
uint8_t Modbus_MasterProcessFrame(uint8_t telegramId, modbus_frame_t* p_frame)
{
    uint8_t status;
    uint8_t idx;
    uint8_t* p_data;
    modbus_telegram_t* p_telegram;

    status = 0;
    p_data = &p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_PAYLOAD];

    if(telegramId < MODBUS_MAX_TELEGRAM)
    {
        p_telegram = (modbus_telegram_t*)&Modbus_Client_Telegram[telegramId];

        if(p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_FUNC_CODE] == p_telegram->funcCode)
        {
            if(p_telegram->p_data != NULL)
            {
                if((p_telegram->funcCode == MODBUS_FUNC_READ_DISCRETE_INPUTS) || (p_telegram->funcCode == MODBUS_FUNC_READ_COILS))
                {
                    if(p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] == p_telegram->qty)
                    {
                        for(idx = 0;idx < p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY];idx++)
                        {
                            *((uint8_t*)p_telegram->p_data + idx) = *p_data;
                            p_data++;
                        }
                        status = 1;
                    }
                }
                else if((p_telegram->funcCode == MODBUS_FUNC_READ_HOLDING_REG) || (p_telegram->funcCode == MODBUS_FUNC_READ_INPUT_REG))
                {
                    if(p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] == (p_telegram->qty * 2))
                    {
                        for(idx = 0;idx < (p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] / 2);idx++)
                        {
                            *(((uint16_t*)(p_telegram->p_data)) + idx) = (uint16_t)*p_data << 8;
                            p_data++;
                            *(((uint16_t*)(p_telegram->p_data)) + idx) |= (uint16_t)*p_data;
                            p_data++;
                        }
                        status = 1;
                    }
                }
            }

            if(p_telegram->pf_callBack != NULL)
            {
                p_telegram->pf_callBack((const uint8_t*)p_data);
            }
        }
    }
    return status;
}

#endif
