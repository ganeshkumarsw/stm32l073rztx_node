/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "hdc1080.h"

/* Global variables ----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Extern variables ----------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c1;

/* Private function prototypes -----------------------------------------------*/

uint8_t HDC1080_Read_Temp(uint16_t* p_outData)
{
    uint8_t buff[2];
    uint8_t status;
    timer_t delay;

    status = 0;

    if(HAL_I2C_Master_Transmit(&hi2c1, HDC1080_WRITE_ADDR, "\x00", 1, 5) == HAL_OK)
    {
        // Conversion time
        Timer_Start(&delay, 10);

        while(Timer_GetStatus(&delay) == TIMER_RUNNING);

        if(HAL_I2C_Master_Receive(&hi2c1, HDC1080_READ_ADDR, buff, sizeof(buff), 10) == HAL_OK)
        {
            *p_outData = ((uint16_t)buff[0] << 8) | (uint16_t)buff[1];
            // Temp in C
            *p_outData = ((*p_outData * 165) / 0xFFFF) - 40;
            status = 1;
        }
    }

    return status;
}

uint8_t HDC1080_Read_Humidity(uint16_t* p_outData)
{
    uint8_t buff[2];
    uint8_t status;
    timer_t delay;

    status = 0;

    if(HAL_I2C_Master_Transmit(&hi2c1, HDC1080_WRITE_ADDR, "\x01", 1, 5) == HAL_OK)
    {
        // Conversion time
        Timer_Start(&delay, 10);

        while(Timer_GetStatus(&delay) == TIMER_RUNNING);

        if(HAL_I2C_Master_Receive(&hi2c1, HDC1080_READ_ADDR, buff, sizeof(buff), 10) == HAL_OK)
        {
            *p_outData = ((uint16_t)buff[0] << 8) | (uint16_t)buff[1];
            // RH in %
            *p_outData = ((*p_outData * 100) / 0xFFFF);
            status = 1;
        }
    }

    return status;
}
