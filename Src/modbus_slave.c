/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "swtimer.h"
#include "modbus_common.h"
#include "modbus_config.h"
#include "modbus_slave.h"

#if MODBUS_SLAVE

/* Global variables ----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

modbus_slave_state_t Modbus_SlaveState;
modbus_frame_t Modbus_SlaveFrame;
timer_t Modbus_SlaveTimeoutTimer;

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

static uint8_t Modbus_SlaveProcessFrame(modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_ReadDescreteInput(uint16_t addr, uint16_t len, modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_ReadCoils(uint16_t addr, uint16_t len, modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_WriteSingleCoil(uint16_t addr, uint16_t val, modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_ReadInputReg(uint16_t addr, uint16_t len, modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_ReadHoldingReg(uint16_t addr, uint16_t len, modbus_frame_t* p_frame);
static modbus_errorcode_t Modbus_WriteSingleReg(uint16_t addr, uint16_t val, modbus_frame_t* p_frame);

/**
 *
 */
void Modbus_SlaveInit(void)
{
#if MODBUS_SLAVE_OVER_RS485
    HAL_GPIO_WritePin(MODBUS_SLAVE_RE_TE_PORT, MODBUS_SLAVE_RE_TE_PIN, GPIO_PIN_SET);
#endif

    HAL_UART_Receive_IT(&MODBUS_SLAVE_UART, Modbus_SlaveFrame.xferBuff, sizeof(Modbus_SlaveFrame.xferBuff));
    
    Timer_Stop(&Modbus_SlaveTimeoutTimer);
    Modbus_SlaveState = MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME;
}

void Modbus_SlaveJob(void)
{
    uint16_t len;
    uint16_t crc;
    modbus_func_t funcCode;

    len = MODBUS_SLAVE_UART.RxXferSize - MODBUS_SLAVE_UART.RxXferCount;

    switch(Modbus_SlaveState)
    {
        case MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME:

            if((len != 0) && (len < 7))
            {
                if(Timer_GetStatus(&Modbus_SlaveTimeoutTimer) == TIMER_STOPPED)
                {
                    Timer_Start(&Modbus_SlaveTimeoutTimer, MODBUS_SLAVE_HEADER_TIMEOUT);
                }

                if(Timer_GetStatus(&Modbus_SlaveTimeoutTimer) == TIMER_ELAPSED)
                {
                    Timer_Stop(&Modbus_SlaveTimeoutTimer);
                    Modbus_SlaveFlushBuffer();
                }
            }
            else if(len > 7)
            {
                Timer_Stop(&Modbus_SlaveTimeoutTimer);

                funcCode = (modbus_func_t)Modbus_SlaveFrame.xferBuff[1];

                if((funcCode == MODBUS_FUNC_READ_DISCRETE_INPUTS)
                   || (funcCode == MODBUS_FUNC_READ_COILS)
                   || (funcCode == MODBUS_FUNC_READ_HOLDING_REG)
                   || (funcCode == MODBUS_FUNC_READ_INPUT_REG))
                {
                    Modbus_SlaveFrame.len = 2 + 4 + 2;
                }
                else if((funcCode == MODBUS_FUNC_WRITE_SINGLE_COILS) || (funcCode == MODBUS_FUNC_WRITE_SINGLE_REG))
                {
                    Modbus_SlaveFrame.len = 2 + 2 + 2 + 2;
                }

                Timer_Start(&Modbus_SlaveTimeoutTimer, Modbus_SlaveFrame.len);

                Modbus_SlaveState = MODBUS_SLAVE_STATE_REQ_FRAME_IN_PROGRESS;
            }
            break;

        case MODBUS_SLAVE_STATE_REQ_FRAME_IN_PROGRESS:

            if(len == Modbus_SlaveFrame.len)
            {
                Timer_Stop(&Modbus_SlaveTimeoutTimer);
                Modbus_SlaveState = MODBUS_SLAVE_STATE_REQ_FRAME_RECEIVED;
            }
            else if(Timer_GetStatus(&Modbus_SlaveTimeoutTimer) == TIMER_ELAPSED)
            {
                Timer_Stop(&Modbus_SlaveTimeoutTimer);

                Modbus_SlaveFlushBuffer();

                Modbus_SlaveState = MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME;
            }

            break;

        case MODBUS_SLAVE_STATE_REQ_FRAME_RECEIVED:

            crc = ((uint16_t)Modbus_SlaveFrame.xferBuff[Modbus_SlaveFrame.len - 1]) << 8
                  | (uint16_t)Modbus_SlaveFrame.xferBuff[Modbus_SlaveFrame.len - 2];

            if(crc == Modbus_CRC16_Calc(Modbus_SlaveFrame.xferBuff, Modbus_SlaveFrame.len - 2))
            {
                Modbus_SlaveState = MODBUS_SLAVE_STATE_PROCESS_REQ_FRAME;
            }
            else
            {
                Modbus_SlaveFlushBuffer();
                Modbus_SlaveState = MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME;
            }
            break;

        case MODBUS_SLAVE_STATE_PROCESS_REQ_FRAME:

            if(Modbus_SlaveProcessFrame(&Modbus_SlaveFrame) == 1)
            {
                Modbus_SlaveState = MODBUS_SLAVE_STATE_RESP_FRAME;
            }
            else
            {
                Modbus_SlaveFlushBuffer();
                Modbus_SlaveState = MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME;
            }
            break;

        case MODBUS_SLAVE_STATE_RESP_FRAME:

#if MODBUS_SLAVE_OVER_RS485
            HAL_GPIO_WritePin(MODBUS_SLAVE_RE_TE_PORT, MODBUS_SLAVE_RE_TE_PIN, GPIO_PIN_RESET);
#endif
            HAL_UART_Transmit_IT(&MODBUS_SLAVE_UART, Modbus_SlaveFrame.xferBuff, Modbus_SlaveFrame.len);
            Modbus_SlaveState = MODBUS_SLAVE_STATE_RESP_FRAME_IN_PROGRESS;
            break;

        case MODBUS_SLAVE_STATE_RESP_FRAME_IN_PROGRESS:
            // Transmit is in progress through interrupt mechanism
            break;

        default:
            break;
    }
}

/**
 *
 */
void Modbus_SlaveFlushBuffer(void)
{
    /* Disable the UART Data Register not empty Interrupt */
    __HAL_UART_DISABLE_IT(&MODBUS_SLAVE_UART, UART_IT_RXNE);

    MODBUS_SLAVE_UART.ErrorCode = HAL_UART_ERROR_NONE;
    MODBUS_SLAVE_UART.pRxBuffPtr = Modbus_SlaveFrame.xferBuff;
    MODBUS_SLAVE_UART.RxXferSize = sizeof(Modbus_SlaveFrame.xferBuff);
    MODBUS_SLAVE_UART.RxXferCount = sizeof(Modbus_SlaveFrame.xferBuff);

    if(MODBUS_SLAVE_UART.RxState == HAL_UART_STATE_READY)
    {
        MODBUS_SLAVE_UART.RxState = HAL_UART_STATE_BUSY_RX;
    }

    /* Clear all the error flag at once */
    __HAL_UART_CLEAR_PEFLAG(&MODBUS_SLAVE_UART);

    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&MODBUS_SLAVE_UART, UART_IT_RXNE);
}

void Modbus_Slave_TxCpltdCallback(void)
{
    Modbus_SlaveFlushBuffer();
#if MODBUS_SLAVE_OVER_RS485
    HAL_GPIO_WritePin(MODBUS_SLAVE_RE_TE_PORT, MODBUS_SLAVE_RE_TE_PIN, GPIO_PIN_SET);
#endif
    Modbus_SlaveState = MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME;
}

/**
 *
 * @param p_frame
 * @return
 */
uint8_t Modbus_SlaveProcessFrame(modbus_frame_t* p_frame)
{
    modbus_errorcode_t errorCode;
    modbus_func_t funcCode;
    uint8_t idx;
    uint8_t status;
    uint8_t slaveId;
    uint16_t addr;
    uint16_t len;
    uint16_t val;
    uint16_t crc;

    status = 1;

    slaveId = p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_SLAVE_ADDR];

    if(slaveId == MODBUS_SLAVE_ADDR)
    {
        funcCode = (modbus_func_t)p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_FUNC_CODE];
        addr = ((uint16_t)p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_ADDR] << 8) | (uint16_t)p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_ADDR + 1];
        len = ((uint16_t)p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_QTY] << 8) | (uint16_t)p_frame->xferBuff[MODBUS_REQ_FRAME_IDX_QTY + 1];

        switch(funcCode)
        {
            case MODBUS_FUNC_READ_COILS:
                errorCode = Modbus_ReadCoils(addr, len, p_frame);
                break;

            case MODBUS_FUNC_READ_DISCRETE_INPUTS:
                errorCode = Modbus_ReadDescreteInput(addr, len, p_frame);
                break;
                        
            case MODBUS_FUNC_WRITE_SINGLE_COILS:
                val = len;
                errorCode = Modbus_WriteSingleCoil(addr, val, p_frame);
                break;

            case MODBUS_FUNC_READ_INPUT_REG:
                errorCode = Modbus_ReadInputReg(addr, len, p_frame);
                break;

            case MODBUS_FUNC_READ_HOLDING_REG:
                errorCode = Modbus_ReadHoldingReg(addr, len, p_frame);
                break;

            case MODBUS_FUNC_WRITE_SINGLE_REG:
                val = len;
                errorCode = Modbus_WriteSingleReg(addr, val, p_frame);
                break;

            default:
                errorCode = MODBUS_ERROR_CODE_FUNC_UNSUPPORTED;
                break;
        }

        if(errorCode != 0)
        {
            idx = MODBUS_RESP_FRAME_IDX_FUNC_CODE;
            p_frame->xferBuff[idx++] = funcCode | 0x80;
            p_frame->xferBuff[idx++] = errorCode;
            p_frame->len = idx;
        }

        crc = Modbus_CRC16_Calc(p_frame->xferBuff, p_frame->len);
        p_frame->xferBuff[p_frame->len++] = crc;
        p_frame->xferBuff[p_frame->len++] = (crc >> 8);
    }
    else
    {
        status = 0;
    }

    return status;
}

/**
 *
 * @param addr
 * @param len
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_ReadDescreteInput(uint16_t addr, uint16_t len, modbus_frame_t* p_frame)
{
    uint8_t pos;
    uint8_t byte;
    uint16_t temp;
    modbus_errorcode_t erroCode;
    modbus_coil_t* p_coil;
    uint8_t* p_data;

    erroCode = (modbus_errorcode_t)0;
    p_data = &p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_PAYLOAD];
    p_coil = (modbus_coil_t*)&Modbus_Server_DescreteInput[addr];
    temp = len;

    if((len > 0) && (len < 2001))
    {
        if((addr + len) <= MODBUS_MAX_COILS)
        {
            if(len % 8)
            {
                len = (len + 8) / 8;
            }

            for(pos = 0; pos < len; pos++)
            {
                p_data[pos] = 0;
            }

            len = temp;

            for(pos = 0; pos < len; pos++)
            {
                if(p_coil->p_data[0] & p_coil->mask)
                {
                    byte = pos/ 8;
                    temp = p_data[byte];
                    p_data[byte] = temp | (1 << (pos & 0x07));
                }
                p_coil++;
            }

            p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] = (len / 8) + 1;
            p_frame->len = MODBUS_RESP_FRAME_IDX_PAYLOAD + p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY];
        }
        else
        {
            erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
        }
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE;
    }

    return erroCode;
}

/**
 *
 * @param addr
 * @param len
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_ReadCoils(uint16_t addr, uint16_t len, modbus_frame_t* p_frame)
{
    uint8_t pos;
    uint8_t byte;
    uint16_t temp;
    modbus_errorcode_t erroCode;
    modbus_coil_t* p_coil;
    uint8_t* p_data;

    erroCode = (modbus_errorcode_t)0;
    p_data = &p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_PAYLOAD];
    p_coil = (modbus_coil_t*)&Modbus_Server_Coils[addr];
    temp = len;

    if((len > 0) && (len < 2001))
    {
        if((addr + len) <= MODBUS_MAX_COILS)
        {
            if(len % 8)
            {
                len = (len + 8) / 8;
            }

            for(pos = 0; pos < len; pos++)
            {
                p_data[pos] = 0;
            }

            len = temp;

            for(pos = 0; pos < len; pos++)
            {
                if(p_coil->p_data[0] & p_coil->mask)
                {
                    byte = pos / 8;
                    temp = p_data[byte];
                    p_data[byte] = temp | (1 << (pos & 0x07));
                }
                p_coil++;
            }

            p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] = (len / 8) + 1;
            p_frame->len = MODBUS_RESP_FRAME_IDX_PAYLOAD + p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY];
        }
        else
        {
            erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
        }
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE;
    }

    return erroCode;
}

/**
 *
 * @param addr
 * @param val
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_WriteSingleCoil(uint16_t addr, uint16_t val, modbus_frame_t* p_frame)
{
    uint8_t idx;
    uint16_t temp;
    modbus_errorcode_t erroCode;
    modbus_coil_t* p_coil;

    erroCode = (modbus_errorcode_t)0;

    if((val == 0) || (val == 0xFF00))
    {
        if((addr < MODBUS_MAX_COILS) && (addr <= 0xFFFF))
        {
            p_coil = (modbus_coil_t*)&Modbus_Server_Coils[addr];
            if(val == 0xFF00)
            {
                p_coil->p_data[0] = p_coil->p_data[0] | p_coil->mask;
            }
            else
            {
                p_coil->p_data[0] = p_coil->p_data[0] & ~p_coil->mask;
            }

            idx = MODBUS_RESP_FRAME_IDX_ADDR;
            if(p_coil->p_data[0] & p_coil->mask)
            {
                temp = 0xFF00;
            }
            else
            {
                temp = 0;
            }
            p_frame->xferBuff[idx++] = addr >> 8;
            p_frame->xferBuff[idx++] = addr;
            p_frame->xferBuff[idx++] = temp >> 8;
            p_frame->xferBuff[idx++] = temp;
            p_frame->len = idx;
        }
        else
        {
            erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
        }
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE;
    }

    return erroCode;
}

/**
 *
 * @param addr
 * @param len
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_ReadInputReg(uint16_t addr, uint16_t len, modbus_frame_t* p_frame)
{
    uint8_t pos;
    uint8_t idx;
    uint16_t temp;
    modbus_errorcode_t erroCode;

    erroCode = (modbus_errorcode_t)0;

    if((len > 0) && (len < 126))
    {
        if(((addr + len) <= MODBUS_MAX_INPUT_REG) && ((addr + len) <= 0xFFFF))
        {
            idx = MODBUS_RESP_FRAME_IDX_PAYLOAD;
            for(pos = 0; pos < len; pos++)
            {
                temp = Modbus_Server_InputReg[addr + pos].p_reg[0];

                p_frame->xferBuff[idx++] = temp >> 8;
                p_frame->xferBuff[idx++] = temp;
            }
            p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] = len * 2;
            p_frame->len = idx;
        }
        else
        {
            erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
        }
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE;
    }

    return erroCode;
}

/**
 *
 * @param addr
 * @param len
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_ReadHoldingReg(uint16_t addr, uint16_t len, modbus_frame_t* p_frame)
{
    uint8_t pos;
    uint8_t idx;
    uint16_t temp;
    modbus_errorcode_t erroCode;

    erroCode = (modbus_errorcode_t)0;

    if((len > 0) && (len < 126))
    {
        if(((addr + len) <= MODBUS_MAX_HOLDING_REG) && ((addr + len) <= 0xFFFF))
        {
            idx = MODBUS_RESP_FRAME_IDX_PAYLOAD;
            for(pos = 0; pos < len; pos++)
            {
                temp = Modbus_Server_HoldingReg[addr + pos].p_reg[0];

                p_frame->xferBuff[idx++] = temp >> 8;
                p_frame->xferBuff[idx++] = temp;
            }
            p_frame->xferBuff[MODBUS_RESP_FRAME_IDX_QTY] = len * 2;
            p_frame->len = idx;
        }
        else
        {
            erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
        }
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE;
    }

    return erroCode;
}

/**
 *
 * @param addr
 * @param val
 * @param p_frame
 * @return
 */
modbus_errorcode_t Modbus_WriteSingleReg(uint16_t addr, uint16_t val, modbus_frame_t* p_frame)
{
    uint8_t idx;
    uint16_t temp;
    modbus_errorcode_t erroCode;

    erroCode = (modbus_errorcode_t)0;

    if((addr < MODBUS_MAX_HOLDING_REG) && (addr <= 0xFFFF))
    {
        idx = MODBUS_RESP_FRAME_IDX_ADDR;
        Modbus_Server_HoldingReg[addr].p_reg[0] = val;
        temp = Modbus_Server_HoldingReg[addr].p_reg[0];
        p_frame->xferBuff[idx++] = addr >> 8;
        p_frame->xferBuff[idx++] = addr;
        p_frame->xferBuff[idx++] = temp >> 8;
        p_frame->xferBuff[idx++] = temp;
        p_frame->len = idx;
    }
    else
    {
        erroCode = MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE;
    }

    return erroCode;
}

#endif
