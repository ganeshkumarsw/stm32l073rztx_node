Configuration	stm32l073rztx_node
STM32CubeMX 	4.21.0
Date	06/09/2017
MCU	STM32L073RZTx



PERIPHERALS	MODES	FUNCTIONS	PINS
I2C1	I2C	I2C1_SCL	PB6
I2C1	I2C	I2C1_SDA	PB7
I2C2	I2C	I2C2_SCL	PB10
I2C2	I2C	I2C2_SDA	PB11
LPUART1	Asynchronous	LPUART1_RX	PC0
LPUART1	Asynchronous	LPUART1_TX	PC1
RCC	Crystal/Ceramic Resonator	RCC_OSC_IN	PH0-OSC_IN
RCC	Crystal/Ceramic Resonator	RCC_OSC_OUT	PH1-OSC_OUT
SYS	Serial_Wire	SYS_SWCLK	PA14
SYS	Serial_Wire	SYS_SWDIO	PA13
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
TIM2	Reset Mode	TIM2_VS_ControllerModeReset	VP_TIM2_VS_ControllerModeReset
TIM2	TI1FP1	TIM2_CH1	PA0
TIM2	Internal Clock	TIM2_VS_ClockSourceINT	VP_TIM2_VS_ClockSourceINT
TIM2	Input Capture direct mode	TIM2_CH1	PA0
TIM2	Input Capture indirect mode	TIM2_CH1	PA0
TIM3	Reset Mode	TIM3_VS_ControllerModeReset	VP_TIM3_VS_ControllerModeReset
TIM3	TI1FP1	TIM3_CH1	PA6
TIM3	Internal Clock	TIM3_VS_ClockSourceINT	VP_TIM3_VS_ClockSourceINT
TIM3	Input Capture direct mode	TIM3_CH1	PA6
TIM3	Input Capture indirect mode	TIM3_CH1	PA6
USART1	Asynchronous	USART1_RX	PA10
USART1	Asynchronous	USART1_TX	PA9



Pin Nb	PINs	FUNCTIONs	LABELs
2	PC13	GPIO_Output	DIP6
5	PH0-OSC_IN	RCC_OSC_IN	
6	PH1-OSC_OUT	RCC_OSC_OUT	
8	PC0	LPUART1_RX	RXD1
9	PC1	LPUART1_TX	TXD1
10	PC2	GPIO_Input	RTS
11	PC3	GPIO_Output	CTS
14	PA0	TIM2_CH1	
22	PA6	TIM3_CH1	
27	PB1	GPIO_Output	DIR1
29	PB10	I2C2_SCL	SCL2
30	PB11	I2C2_SDA	SDA2
37	PC6	GPIO_Output	RST
38	PC7	GPIO_Output	CONF
39	PC8	GPIO_Output	DIP1
40	PC9	GPIO_Output	DIP2
42	PA9	USART1_TX	TXD2
43	PA10	USART1_RX	RXD2
46	PA13	SYS_SWDIO	
49	PA14	SYS_SWCLK	
51	PC10	GPIO_Output	DIP3
52	PC11	GPIO_Output	DIP4
53	PC12	GPIO_Output	DIP5
55	PB3	GPIO_Output	DIR2
56	PB4	GPIO_Output	DIR3
58	PB6	I2C1_SCL	SCL1
59	PB7	I2C1_SDA	SDA1



SOFTWARE PROJECT

Project Settings : 
Project Name : stm32l073rztx_node
Project Folder : D:\Ganesh\Gitlab\stm32l073rztx_node
Toolchain / IDE : MDK-ARM V5
Firmware Package Name and Version : STM32Cube FW_L0 V1.9.0


Code Generation Settings : 
STM32Cube Firmware Library Package : Copy only the necessary library files
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : No
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : Balanced Size/Speed






