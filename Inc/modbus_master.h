/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_MASTER__H
#define __MODBUS_MASTER__H

/* Includes ------------------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define MODBUS_MASTER_RESP_HEADER_TIMEOUT       5

/* Private enum --------------------------------------------------------------*/

/* Private typedef------------------------------------------------------------*/

typedef enum
{
    MODBUS_MSTR_STATE_REQ_FRAME = 0,
    MODBUS_MSTR_STATE_REQ_FRAME_IN_PROGRESS = 1,
    MODBUS_MSTR_STATE_WAIT_FOR_RESP_FRAME = 2,
    MODBUS_MSTR_STATE_RESP_FRAME_IN_PROGRESS = 3,
    MODBUS_MSTR_STATE_RESP_FRAME_RECEIVED = 4,
    MODBUS_MSTR_STATE_PROCESS_RESP_FRAME = 5,
}modbus_master_state_t;

/* Exported functions --------------------------------------------------------*/

void Modbus_MasterInit(void);
void Modbus_MasterJob(void);
void Modbus_MasterFlushBuffer(void);
void Modbus_Master_TxCpltdCallback(void);

#endif /* __MODBUS_H */
