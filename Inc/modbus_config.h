/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_CONFIG_H
#define __MODBUS_CONFIG_H

/* Includes ------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define MODBUS_MASTER                   1
#define MODBUS_SLAVE                    1

#define MODBUS_SLAVE_ADDR               10

/* Extern variables ----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define MODBUS_SLAVE_OVER_RS485             1
#define MODBUS_SLAVE_RE_TE_PORT             DIR2_GPIO_Port
#define MODBUS_SLAVE_RE_TE_PIN              DIR2_Pin
#define MODBUS_SLAVE_UART                   hlpuart1

#define MODBUS_MSTR_OVER_RS485              1
#define MODBUS_MSTR_RE_TE_PORT              DIR3_GPIO_Port
#define MODBUS_MSTR_RE_TE_PIN               DIR3_Pin
#define MODBUS_MSTR_UART                    huart1

#define MODBUS_MAX_DESCRETE_INPUT       10
#define MODBUS_MAX_COILS                10
#define MODBUS_MAX_INPUT_REG            10
#define MODBUS_MAX_HOLDING_REG          1000

#define MODBUS_MAX_TELEGRAM             1

/* Private enum --------------------------------------------------------------*/


/* Private typedef -----------------------------------------------------------*/

/* Extern Variables ----------------------------------------------------------*/
extern const modbus_coil_t Modbus_Server_DescreteInput[MODBUS_MAX_DESCRETE_INPUT];
extern const modbus_coil_t Modbus_Server_Coils[MODBUS_MAX_COILS];
extern const modbus_reg_t Modbus_Server_HoldingReg[MODBUS_MAX_HOLDING_REG];
extern const modbus_reg_t Modbus_Server_InputReg[MODBUS_MAX_INPUT_REG];

extern const modbus_telegram_t Modbus_Client_Telegram[MODBUS_MAX_TELEGRAM];
extern uint16_t Modbus_Client_U16_Coil;
extern uint16_t Modbus_Client_U16[10];

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef hlpuart1;

#endif /* __MODBUS_H */
