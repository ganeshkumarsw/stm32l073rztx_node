/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HDC1080_H
#define __HDC1080_H

/* Includes ------------------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define HDC1080_ADDR        0x40
#define HDC1080_READ_ADDR   0x80
#define HDC1080_WRITE_ADDR  0x81

/* Private enum --------------------------------------------------------------*/


/* Private typedef------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
uint8_t HDC1080_Read_Temp(uint16_t* p_outData);
uint8_t HDC1080_Read_Humidity(uint16_t* p_outData);

#endif /* __HDC1080_H */
