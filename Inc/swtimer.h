/* 
 * File:   swtimer.h
 * Author: Ganeshkumar
 *
 * Created on 15. Oktober 2015, 11:40
 */

#ifndef _SWTIMER_H
#define	_SWTIMER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
/* Includes ------------------------------------------------------------------*/

    
/* Exported types ------------------------------------------------------------*/
typedef enum
{
    TIMER_STOPPED = 0,
    TIMER_RUNNING,
    TIMER_ELAPSED,
}timer_status_t;

/// Definition for software timer
typedef struct
{
    uint32_t timeStamp;
    uint32_t delay;
}timer_t;

/* Exported functions --------------------------------------------------------*/
void Timer_Initialize(void);
void Timer_Start(timer_t *p_timer, uint32_t delay);
void Timer_Stop(timer_t *p_timer);
timer_status_t Timer_GetStatus(timer_t *p_timer);

#ifdef	__cplusplus
}
#endif

#endif	/* __SWTIMER_H */

