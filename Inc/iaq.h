/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __IAQ_H
#define __IAQ_H

/* Includes ------------------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define IAQ_ADDR        0x5A
#define IAQ_READ_ADDR   0xB5
#define IAQ_WRITE_ADDR  0xB4

/* Private enum --------------------------------------------------------------*/
typedef enum
{
    IAQ_READ_CO2,
    //IAQ_READ_STATUS,
    IAQ_READ_RESISTANCE,
    IAQ_READ_TVOC,
}iaq_read_t;

/* Private typedef------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
uint8_t IAQ_Read(iaq_read_t type, void* p_outData);

#endif /* __IAQ_H */
