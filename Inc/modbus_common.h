/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_COMMON_H
#define __MODBUS_COMMON_H

/* Includes ------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define MODBUS_REQ_FRAME_IDX_SLAVE_ADDR    0
#define MODBUS_REQ_FRAME_IDX_FUNC_CODE     1
#define MODBUS_REQ_FRAME_IDX_ADDR          2
#define MODBUS_REQ_FRAME_IDX_QTY           4
#define MODBUS_REQ_FRAME_IDX_PAYLOAD       6

#define MODBUS_RESP_FRAME_IDX_SLAVE_ADDR    0
#define MODBUS_RESP_FRAME_IDX_FUNC_CODE     1
#define MODBUS_RESP_FRAME_IDX_QTY           2
#define MODBUS_RESP_FRAME_IDX_PAYLOAD       3
#define MODBUS_RESP_FRAME_IDX_ADDR          2

/* Extern variables ----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private enum --------------------------------------------------------------*/
typedef enum
{
    MODBUS_FUNC_READ_COILS = 1,
    MODBUS_FUNC_READ_DISCRETE_INPUTS = 2,
    MODBUS_FUNC_WRITE_SINGLE_COILS =5,
    MODBUS_FUNC_WRITE_MUL_COILS = 15,
    MODBUS_FUNC_READ_INPUT_REG = 4,
    MODBUS_FUNC_READ_HOLDING_REG = 3,
    MODBUS_FUNC_WRITE_SINGLE_REG = 6,
    MODBUS_FUNC_WRITE_MUL_REG = 10,
    MODBUS_FUNC_RW_MUL_REG = 17,
}modbus_func_t;

typedef enum
{
    MODBUS_ERROR_CODE_FUNC_UNSUPPORTED = 1,
    MODBUS_ERROR_CODE_ADDR_OUT_OF_RANGE = 2,
    MODBUS_ERROR_CODE_QTY_OUT_OF_RANGE = 3,
}modbus_errorcode_t;

/* Private typedef -----------------------------------------------------------*/

typedef struct
{
    uint8_t mask;
    uint8_t* p_data;
}modbus_coil_t;

typedef struct
{
    uint16_t* p_reg;
}modbus_reg_t;

typedef struct
{
    uint8_t xferBuff[300];
    uint16_t len;
}modbus_frame_t;

typedef struct
{
    uint8_t slaveAddr;
    uint8_t funcCode;
    uint16_t addr;
    uint16_t qty;
    void* p_data;
    void (*pf_callBack)(const uint8_t*);
}modbus_telegram_t;

/* Extern Variables ----------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
uint16_t Modbus_CRC16_Calc(uint8_t* p_buf, uint16_t len);

#endif /* __MODBUS_H */
