/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_RTU_H
#define __MODBUS_RTU_H

/* Includes ------------------------------------------------------------------*/


/* Private enum --------------------------------------------------------------*/

/* Private typedef------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
void Modbus_RTU_Init(void);
void Modbus_RTU_Job(void);

#endif /* __MODBUS_H */
