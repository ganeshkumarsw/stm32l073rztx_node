/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DUST_H
#define __DUST_H

/* Includes ------------------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/


/* Private enum --------------------------------------------------------------*/

/* Private typedef------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
void DUST_Init(void);
uint16_t DUST_Read_P1(void);
uint16_t DUST_Read_P2(void);

#endif /* __IAQ_H */
