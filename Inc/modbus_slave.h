/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_SLAVE__H
#define __MODBUS_SLAVE__H

/* Includes ------------------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define MODBUS_SLAVE_HEADER_TIMEOUT           10

/* Private enum --------------------------------------------------------------*/

/* Private typedef------------------------------------------------------------*/

typedef enum
{
    MODBUS_SLAVE_STATE_WAIT_FOR_REQ_FRAME = 0,
    MODBUS_SLAVE_STATE_REQ_FRAME_IN_PROGRESS = 1,
    MODBUS_SLAVE_STATE_REQ_FRAME_RECEIVED = 2,
    MODBUS_SLAVE_STATE_PROCESS_REQ_FRAME = 3,
    MODBUS_SLAVE_STATE_RESP_FRAME = 4,
    MODBUS_SLAVE_STATE_RESP_FRAME_IN_PROGRESS = 5,
}modbus_slave_state_t;

/* Exported functions --------------------------------------------------------*/

void Modbus_SlaveInit(void);
void Modbus_SlaveJob(void);
void Modbus_SlaveFlushBuffer(void);
void Modbus_Slave_TxCpltdCallback(void);

#endif /* __MODBUS_H */
